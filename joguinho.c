#include <stdlib.h>
#include <stdio.h>
#include <allegro5/allegro.h>
#include <unistd.h>
#include <allegro5/allegro_image.h>
#include <unistd.h>
#include <time.h>

typedef struct porta{
  int x;
  int y;
}Porta;

typedef struct personagem{
    char Nome[500];
    int fase;
    struct personagem *prox;
}Personagem;

typedef struct charada{
    char pergunta[500];
    char resposta[100];
    int certo;
}Charada;


Personagem *alocaJogadorNovo(char *nome,int fase){
    Personagem *new;
    new = (Personagem*)malloc(sizeof(Personagem));
    strcpy(new->Nome,nome);
    new->fase = fase;
    new->prox= NULL;
    return new;
}

Personagem *salvaNaLista(Personagem p,Personagem *lst){
  int teste;
  Personagem aux = p;
  Personagem *list = lst;
  while(list!=NULL){
      if(strcmp(aux.Nome,list->Nome)==0){teste=1;break;}
      else list = list->prox;
  }
  if(teste == 1){
    list->fase = p.fase;
    return lst;
  }
  else{
    Personagem *novo = (Personagem*)malloc(sizeof(Personagem));  // Função para inserir elementos em uma lista encadeada
    strcpy(novo->Nome,p.Nome);                           // Adiciona no inicio da fila
    novo->fase = p.fase;
    novo->prox=lst;
    return novo;
  }
}

Personagem *carregaLista(Personagem *lst){
  FILE *fp = fopen("save.bin","rb");
  if(fp!=NULL){
      char nome[500];
      Personagem novo[1];
      while(fread(novo,sizeof(Personagem),1,fp)==1){
          lst = salvaNaLista(novo[0],lst);
      }
  }
  return lst;
}

void imprime_lista(Personagem *lst){ // imprimir os personagens que foram carregados
    Personagem *aux = lst;
    while(aux!=NULL){
        printf("O nome do personagem:\n %s\n",aux-> Nome);
        aux = aux->prox;
    }
}

void salvaListaArq(Personagem *lst){
  FILE *fp = fopen("save.bin","wb");
  imprime_lista(lst);
  Personagem*aux=lst;
  while(aux!=NULL){
      fwrite(aux,sizeof(Personagem),1,fp);
      aux = aux->prox;
  }
  fclose(fp);

}

Personagem *filtraLista(Personagem *lst){
  Personagem *aux = lst;
  char nome[500];
  imprime_lista(lst);
  printf("Digite o nome do personagem!\n");
  scanf(" %499[^\n]",nome);
  while(aux!=NULL){
      if(strcmp(aux->Nome,nome)==0)return alocaJogadorNovo(nome,aux->fase);
      else aux = aux->prox;
  }
  return NULL;
}

int detectaMouse(int x,int y,int linha,int coluna){
  int teste=0;
  if(x>=((coluna-1)*120))teste++;
  if(x<=(coluna*120))teste++;
  if(y>=((linha-1)*80))teste++;
  if(y<=(linha*80))teste++;

  if(teste==4)return 1;
  else return 0;
}

void loadAnim(ALLEGRO_BITMAP*background,ALLEGRO_BITMAP*door1,ALLEGRO_BITMAP *door2,ALLEGRO_BITMAP *door3,ALLEGRO_BITMAP *door4,int statex,int statey,Porta *p,int i,int j){
 if(i==5 && j ==3 ){

   al_draw_bitmap(background,0,0,0);
   al_draw_bitmap(door2,statex*120,statey*80,0);
   al_draw_bitmap(door1,p[1].x,p[1].y,0);
   al_draw_bitmap(door1,p[2].x,p[2].y,0);
   al_flip_display();
   al_rest(0.1);

   al_draw_bitmap(background,0,0,0);
   al_draw_bitmap(door3,statex*120,statey*80,0);
   al_draw_bitmap(door1,p[1].x,p[1].y,0);
   al_draw_bitmap(door1,p[2].x,p[2].y,0);
   al_flip_display();
   al_rest(0.1);

   al_draw_bitmap(background,0,0,0);
   al_draw_bitmap(door4,statex*120,statey*80,0);
   al_draw_bitmap(door1,p[1].x,p[1].y,0);
   al_draw_bitmap(door1,p[2].x,p[2].y,0);
   al_flip_display();
   al_rest(0.1);

 }
 else if (i==5 && j ==4){

   al_draw_bitmap(background,0,0,0);
   al_draw_bitmap(door2,statex*120,statey*80,0);
   al_draw_bitmap(door1,p[0].x,p[0].y,0);
   al_draw_bitmap(door1,p[2].x,p[2].y,0);
   al_flip_display();
   al_rest(0.1);

   al_draw_bitmap(background,0,0,0);
   al_draw_bitmap(door3,statex*120,statey*80,0);
   al_draw_bitmap(door1,p[0].x,p[0].y,0);
   al_draw_bitmap(door1,p[2].x,p[2].y,0);
   al_flip_display();
   al_rest(0.1);

   al_draw_bitmap(background,0,0,0);
   al_draw_bitmap(door4,statex*120,statey*80,0);
   al_draw_bitmap(door1,p[0].x,p[0].y,0);
   al_draw_bitmap(door1,p[2].x,p[2].y,0);
   al_flip_display();
   al_rest(0.1);
 }
 else{

  al_draw_bitmap(background,0,0,0);
  al_draw_bitmap(door2,statex*120,statey*80,0);
  al_draw_bitmap(door1,p[0].x,p[0].y,0);
  al_draw_bitmap(door1,p[1].x,p[1].y,0);
  al_flip_display();
  al_rest(0.1);

  al_draw_bitmap(background,0,0,0);
  al_draw_bitmap(door3,statex*120,statey*80,0);
  al_draw_bitmap(door1,p[0].x,p[0].y,0);
  al_draw_bitmap(door1,p[1].x,p[1].y,0);
  al_flip_display();
  al_rest(0.1);

  al_draw_bitmap(background,0,0,0);
  al_draw_bitmap(door4,statex*120,statey*80,0);
  al_draw_bitmap(door1,p[0].x,p[0].y,0);
  al_draw_bitmap(door1,p[1].x,p[1].y,0);
  al_flip_display();
  al_rest(0.1);
}
}

int **carregaHitbox(){
    FILE *fp;
    char url[] = "hitbox.txt";
    int i;
    int **hitbox;
    hitbox = (int**)malloc(7*sizeof(int*));
    for(i=0;i<7;i++){
        hitbox[i] = (int*)malloc(7*sizeof(int));
    }
    i=0;
    fp=fopen(url,"r");
    while(fscanf(fp,"%d %d %d %d %d %d %d",&hitbox[i][0],&hitbox[i][1],&hitbox[i][2],&hitbox[i][3],&hitbox[i][4],&hitbox[i][5],&hitbox[i][6])==7){
      i++;
    }
    return hitbox;
}

int loopAnimation(Porta *portas,int **hitbox){
    ALLEGRO_MOUSE_STATE mice;
    ALLEGRO_KEYBOARD_STATE key;
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_BITMAP *door1  = NULL;
    ALLEGRO_BITMAP *door2  = NULL;
    ALLEGRO_BITMAP *door3  = NULL;
    ALLEGRO_BITMAP *door4  = NULL;
    ALLEGRO_BITMAP *background = NULL;
    al_init();
    al_init_image_addon();
    al_install_keyboard();
    if(!al_install_mouse())printf("erro!\n");

    display = al_create_display(840,560); // 840 linhas por 560 colunas
    background = al_load_bitmap("Imagens/background.png");
    door1 = al_load_bitmap("Imagens/door_closed.png"); //120 colunas por 80 linhas
    door2 = al_load_bitmap("Imagens/door_op1.png");
    door3 = al_load_bitmap("Imagens/door_op2.png");
    door4 = al_load_bitmap("Imagens/door_op3.png");
    al_draw_bitmap(background,0,0,0);
    al_draw_bitmap(door1,portas[0].x,portas[0].y,0); // coluna por linha
    al_draw_bitmap(door1,portas[1].x,portas[1].y,0);
    al_draw_bitmap(door1,portas[2].x,portas[2].y,0);
    al_flip_display();
    int i,j,anim=0,statex=1,statey=1,x1=1,y1=1;
    while(1){
      al_get_keyboard_state(&key);
      al_get_mouse_state(&mice);
      for(i=1;i<7;i++){
        for(j=1;j<7;j++){
            if(detectaMouse(mice.x,mice.y,i,j) && hitbox[i][j]==1 && anim==0){
              anim = 1 ;
              x1=j;
              statex = j-1;
              y1=i;
              statey=i-1;
              loadAnim(background,door1,door2,door3,door4,statex,statey,portas,i,j);

            }
            else if (!detectaMouse(mice.x,mice.y,y1,x1)&& anim==1){
              anim = 0;
              al_draw_bitmap(background,0,0,0);
              al_draw_bitmap(door1,portas[0].x,portas[0].y,0);
              al_draw_bitmap(door1,portas[1].x,portas[1].y,0);
              al_draw_bitmap(door1,portas[2].x,portas[2].y,0);
              al_flip_display();
            }
        }
      }
      if(al_mouse_button_down(&mice,1)){
        for(i=1;i<7;i++){
          for(j=1;j<7;j++){
              if(detectaMouse(mice.x,mice.y,i,j) && hitbox[i][j]==1){
                  if(i == 3 && j == 3){al_destroy_display(display);al_uninstall_mouse();return 1;}
                  else if(i==4 && j==4){al_destroy_display(display);al_uninstall_mouse();return 2;}
                  else {al_destroy_display(display);al_uninstall_mouse();return 3;}
              }
          }
        }
      }
      if(al_key_down(&key,ALLEGRO_KEY_ESCAPE)){
          al_destroy_display(display);
          return 0;
      }
  }
}

int charada(int escolha, Personagem p){
    int resposta,aleat,i=0;
    srand(time(NULL));
    FILE *fp;
    Charada c;
    fp = fopen("charadas.bin","rb");
    printf("Voce escolheu a porta %d e ao abri-la",escolha);
    printf(" encontrou um papel que continha uma charada,resolva para prosseguir\n");
    if(p.fase == 1){
        aleat = (rand() % 3);
        fseek(fp,(aleat*sizeof(Charada)),SEEK_SET);
        fread(&c,sizeof(Charada),1,fp);
        printf("%s \n",c.pergunta);
        printf("%s \n",c.resposta);
        scanf("%d",&resposta);
        if(resposta==(c.certo))return 1;
        else return 0;

    }
    else if(p.fase == 2){
      aleat = (rand() % 3) + 3;
      fseek(fp,(aleat*sizeof(Charada)),SEEK_SET);
      fread(&c,sizeof(Charada),1,fp);
      printf("%s \n",c.pergunta);
      printf("%s \n",c.resposta);
      scanf("%d",&resposta);
      if(resposta==(c.certo))return 1;
      else return 0;

    }
    else if(p.fase==3){
      aleat = (rand()%3) + 6 ;
      fseek(fp,(aleat*sizeof(Charada)),SEEK_SET);
      fread(&c,sizeof(Charada),1,fp);
      printf("%s \n",c.pergunta);
      printf("%s \n",c.resposta);
      scanf("%d",&resposta);
      if(resposta==(c.certo))return 1;
      else return 0;
    }
	
	else if(p.fase==4){
      aleat = 9;
      fseek(fp,(aleat*sizeof(Charada)),SEEK_SET);
      fread(&c,sizeof(Charada),1,fp);
      printf("%s \n",c.pergunta);
      printf("%s \n",c.resposta);
      scanf("%d",&resposta);
      if(resposta==(c.certo))return 1;
      else return 0;
    }

    fclose(fp);
}

int escolha(){
  int resp ;
  printf("Voce conseguiu passar pelo desafio, o que deseja fazer ? \n");
  printf("[1]Para prosseguir sua jornada\n");
  printf("[2]Para salvar o jogo\n");
  printf("[3]Para desistir\n");
  scanf("%d",&resp);
  return resp;
}

int main(int argc, char const *argv[]) {


  Porta portas[3];

  portas[0].x = 240;
  portas[0].y = 320;

  portas[1].x = 360 ;
  portas[1].y= 320;

  portas[2].x = 480;
  portas[2].y = 320;

  int resp,**hitbox,porta,continuar;
  Personagem *p;
  Personagem *lst = NULL;
  lst=carregaLista(lst);
  if(lst==NULL) printf("Falha ao abrir o arquivo de personagens\n");
  hitbox = carregaHitbox();

  printf("O que deseja fazer,aventureiro?\n");
  printf("[1]Para criar novo personagem\n");
  printf("[2]Para carregar um personagem\n");
  printf("[3]Para cancelar sua aventura\n");
  scanf("%d",&resp);
  if(resp==3) return 1;
  else if(resp == 1){
      char Nome[500];
      printf("Me fale o seu nome aventureiro:\n");
      scanf(" %499[^\n]",Nome);
      p = alocaJogadorNovo(Nome,1);
      printf("Num reino distante, chamado Azgaroth, havia uma lenda em que nesse reino, havia a princesa ");
      printf("mais bela de todo planeta Terra, seu nome era Taka.\nE foi assim que tudo começou...\n");
      usleep(0);
      printf("Me chamo %s, e estou a caminho de Azgaroth, para encontrar a tão sonhada princesa Taka\n",Nome);
      printf("fazem horas que estou na estrada, e já estou muito cansado...\n\n");
      usleep(0);
      printf("Meu deus, que barulho é esse? Acho que tem alguém me perseguindo");
      printf(".\n.\n.\n.\n");
      usleep(0);
      system("clear");
      printf("Onde estou? Que lugar é esse?\n");
      usleep(0);
      printf("Você está no calabouço de Azgaroth, me chamo Venke, e sou o protetor dessa cidade\n");
      printf("nós nunca te vimos, te capturamos, e trouxemos pra cá, mas fique calmo, que você pode sair daqui..\n");
      usleep(0);
      printf("Basta resolver 3 charadas, e você terá seu acesso permitido a gloriosa cidade de Azgaroth!\n\n");
      usleep(0);
      printf("OK, sou um guerreiro e um estudioso assíduo, acho que isso não será problema pra mim.\n");
      printf(".\n.\n.\n Me mostre as charadas!!");
      usleep(0);
      }
  else{
      p=filtraLista(lst);
      if(p==NULL) {printf("Personagem n pode ser encontrado\n");return 0;}
  }
  while(1){
       printf("Vc está na fase %d\n",p->fase);
       porta=loopAnimation(portas,hitbox);
       if(porta==0) break;
       resp=charada(porta,*p);
       if(resp==1){
          p->fase++;
      }
       else {printf("fim de jogo\n");return 0;}
       resp = escolha();
       if(resp==2){
         lst=salvaNaLista(*p,lst);
         salvaListaArq(lst);
         printf("Personagem salvo ! deseja continuar a jogar?[1]sim[2]nao\n");
         scanf("%d",&continuar);
         if(continuar==2) return 0;
      }
       else if(resp==3) return 0 ;
  }
  return 0;
}
