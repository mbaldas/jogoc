#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct charada{
    char pergunta[500];
    char resposta[100];
    int certo;
}Charada;

int main(){
  Charada c[10];
  FILE *fp;
  fp = fopen("charadas.bin","wb");

  strcpy(c[0].pergunta,"Qual edificio tem mais historia que qualquer outra?");
  strcpy(c[0].resposta,"[1]uff [2]predio [3]biblioteca");
  c[0].certo = 3;

  strcpy(c[1].pergunta,"Oque e oque e, nao se come mas e bom pra comer?");
  strcpy(c[1].resposta,"[1]talher [2]colher [3]garfo");
  c[1].certo = 1;

  strcpy(c[2].pergunta,"Oque e oque, vai pra cima e pra baixo mas nao se move?");
  strcpy(c[2].resposta,"[1]tempo [2]predio [3]escada");
  c[2].certo = 3;

  strcpy(c[3].pergunta,"Tem 5 irmãs, Ana está lendo um livro, Julia cozinhando, Catia jogando xadrez, e Maria ouvindo música, onde está a quinta?");
  strcpy(c[3].resposta,"[1]lendo [2]jogando [3]lavando");
  c[3].certo = 2;

  strcpy(c[4].pergunta,"Oque e oque e, ficamos em pe, ele fica deitado, ficamos deitado ele fica em pé?");
  strcpy(c[4].resposta,"[1]pe [2]braco [3]cama");
  c[4].certo = 1;

  strcpy(c[5].pergunta,"O que é quebrado sem ser mantido?");
  strcpy(c[5].resposta,"[1]amor [2]perda [3]promessa");
  c[5].certo = 3;

  strcpy(c[6].pergunta,"Essa coisa te devora, passáros, monstros, arvores. Reduz em pó as rochas mais resistentes. Mata o rei e destrói as cidades. Oque é?");
  strcpy(c[6].resposta,"[1]exercito [2]tempo [3]armas");
  c[6].certo = 2;

  strcpy(c[7].pergunta,"É mais leve que uma pena, mas a maioria, mesmos mais fortes, não podem segurar por mais de alguns minutos. Oque é?");
  strcpy(c[7].resposta,"[1]culpa [2]dor [3]odio");
  c[7].certo = 1;

  strcpy(c[8].pergunta,"Quanto menos luz mais eu apareço. Quem eu sou?");
  strcpy(c[8].resposta,"[1]escuridao [2]claridade [3]paz");
  c[8].certo = 1;

  strcpy(c[9].pergunta,"Quando voce falar, eu sumirei");
  strcpy(c[9].resposta,"[1]consciência [2]silencio [3]barulho");
  c[9].certo = 2;

  int i;
  for(i=0;i<10;i++){
    fwrite(&c[i],sizeof(Charada),1,fp);
  }
  fclose(fp);
  fp=fopen("charadas.bin","rb");
  Charada x;
  fseek(fp,(9*sizeof(Charada)),SEEK_SET);
  fread(&x,sizeof(Charada),1,fp);
  printf("%s\n",x.pergunta);
}
